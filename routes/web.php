<?php
 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(); 

Route::group(['middleware' => ['auth']], function () {

	Route::get('/home', 'HomeController@index')->name('home');
 
   Route::any('productImport','Api\ImportProductController@index')->name('importForm');


   Route::any('import','Api\ImportProductController@importProduct')->name('productimport');

	//Route::any('productList','Api\ImportProductController@productList')->name('productList');

    Route::any('productDelete','Api\ImportProductController@productDelete')->name('productDelete');
    Route::any('delete','Api\ImportProductController@delete')->name('delete');
  
	Route::any('updateShopifyProductVariant','Api\ImportProductController@updateShopifyProductVariant');

      Route::any('productDetails','Api\ProductUpdateController@index');


    /*============= CSV import into database ============*/
    Route::any('importCsv','SupplierController@importCsv');
    Route::any('saveCsv','SupplierController@saveCsv')->name('saveCsv');
    Route::any('importCsvList','SupplierController@importCsvList')->name('importCsvList');

    Route::any('test','Api\ImportProductController@imageUpload');

});

Route::any('getAllProductPrice','SupplierController@getAllProductPrice');

Route::any('/listShopifyProduct', 'TestController@listShopifyProduct')->name('productslist');
Route::any('/updateShopifyProduct', 'TestController@updateShopifyProduct')->name('productsupdate');
Route::any('/createTestOrderOnSupplier', 'TestController@createTestOrderOnSupplier')->name('createorder');







