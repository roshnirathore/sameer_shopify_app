<?php

namespace App\Services\DanJewellers;

use GuzzleHttp;

class OrderAPI
{
    private $username;
    private $password;
    private $client;


    public function __construct($config, $client = null)
    {  
        $this->username = $config['danjewellers_username'];
        $this->password = $config['danjewellers_password'];
        $this->client = $client ?: new GuzzleHttp\Client([
            'base_uri' => $config['danjewellers_password_base_uri'],
            'query'   => ['UserName' => $this->username, 'Password' => $this->password,'UseForm'=> true],
            'headers' => [
               "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
            ]
        ]);
    }
   
   /*Init API*/
   public static function init(){
        return new OrderAPI([
            'danjewellers_username' => config('thirdparty.danjewellers_username'),
            'danjewellers_password' => config('thirdparty.danjewellers_password'),
            'danjewellers_password_base_uri' => config('thirdparty.danjewellers_password_base_uri'),
        ]);
   }


   /*Create Order*/
   public function createOrder($order){  
      
        try {
           $response = $this->client->post('/PutOrder.aspx', ['form_params'=>$order]);
           return $response->getBody()->getContents();
          return array('success' => true, 'orderId' => $response->getBody()->getContents());
        }catch (\Exception $e) {
          return array('success' => false, 'error' => $e->getMessage());
        }
   }  
}
