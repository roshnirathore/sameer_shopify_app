<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\Services\Shopify\ProductAPI;

class ImportProductController extends Controller
{ 
  public function __construct(){
        $this->middleware('auth');
        $this->shopifyapi = ProductAPI::init();
    }

    //Display from
    public function index(){
      return view('shopifyProduct.import'); 
    }
    //import product in shopify as well database
    public function importProduct(Request $request){
      $request->validate([
            'file' => 'required'
        ], [
            'file.required' => 'Please select product csv file'
        ]);
       $file = $request->file('file');
        if($file){
          $extension = $file->getClientOriginalExtension();
          $fileName  = $file->getFilename().'.'.$extension;
          $success = Storage::disk('public')->put($fileName ,  File::get($file));
          $filePath = public_path($fileName);

          if($filePath){
            $csvData=array_map('str_getcsv',file($file));
            $csvHeader = $csvData[0];
            unset($csvData[0]);

            $parentProducts = array();
            $childProducts = array();

            /*===== csv file convert into array start ======*/
            foreach($csvData as $row){
              $product = array_combine($csvHeader, $row);
              if(empty(trim($product['parent product Id']))){
                  $parentProducts[] = $product;
              }
              if(!empty(trim($product['parent product Id'])) && trim($product['parent product Id']) > 0){
                  $childProducts[trim($product['parent product Id'])][] = $product;
              }  
            }
           /*==== csv file convert into array start  =========*/

            /*===== parent and child array combine in one array start ======*/
            for ($i=0; $i < count($parentProducts) ; $i++) { 
              $parentId = $parentProducts[$i]['productId'];
              if (array_key_exists($parentId,$childProducts)){
                array_push($parentProducts[$i],$childProducts[$parentId]);  
              }
            }
             /*======= parent and child array combine in one array start ======*/


            for ($i=0; $i < count($parentProducts); $i++) { 
              $productPayload = array();

              $productPayload['id'] = $parentProducts[$i]['productId'];
              /*====== select variant id by product id from database ======*/
              $product_id = $productPayload['id'];
              $product = Product::where('product_id',$product_id)->get();
              $product_arr = $product->toArray();
              if(!empty($product_arr)){

                $product_variant_id  = $product_arr[0]['shopify_product_variant_id'];

                if(isset($parentProducts[$i][0])){
                  /*====== product variant loop start ========*/
                  for ($j=0; $j <= count($parentProducts[$i][0]); $j++) {

                    $product_id = $parentProducts[$i][0][$j]['productId'];

                    $product = Product::where('product_id',$product_id)->get();
                    $product_arr = $product->toArray();
                    if(!empty($product_arr)){

                      $product_variant_id  = $product_arr[0]['shopify_product_variant_id'];

                      $weight = $parentProducts[$i][0][$j]['Weight'];
                      $height = $parentProducts[$i][0][$j]['Height'];
                      $depth = $parentProducts[$i][0][$j]['Depth'];

                      //call update shopify api endpoint function
                      $this->updateShopifyProductVariant($product_variant_id,$weight,$height,$depth);
                    }
                  }
                  /*product variant loop end*/
                }
                $weight = $parentProducts[$i]['Weight'];
                $height = $parentProducts[$i]['Height'];
                $depth = $parentProducts[$i]['Depth'];

                //call update shopify api endpoint function
                $this->updateShopifyProductVariant($product_variant_id,$weight,$height,$depth);

              }
              else{
              $productPayload['title'] = addslashes($parentProducts[$i]['Product Name']);
              $productPayload['body_html'] = preg_replace('/[^a-zA-Z0-9_ -]/s', '',$parentProducts[$i]['LongDescription']);
              $productPayload['options'] = array(array("name"=> "Weight"),
                            array("name"=> "Height"),
                            array("name"=> "Depth"));
              $productPayload['variants'] = array();

              if(isset($parentProducts[$i][0])){
            
                for ($j=0; $j <= count($parentProducts[$i][0]); $j++) { 
                  if($j == count($parentProducts[$i][0])){
                    $data = array( 
                      "id" =>$parentProducts[$i]['productId'],
                      "parent_id" => $parentProducts[$i]['productId'],
                      "option1" =>$parentProducts[$i]['Weight'],
                      "option2" =>$parentProducts[$i]['Height'],
                      "option3" =>$parentProducts[$i]['Depth'],
                      "inventory_management"=>"shopify",
                      "position" => "1",
                      "price" => $parentProducts[$i]['PricePerPiece'],
                      "inventory_quantity" => "3");
                      array_push($productPayload['variants'],$data);
                  }else{
                    $data = array( 

                      "id" =>$parentProducts[$i][0][$j]['productId'],
                      "parent_id" => $parentProducts[$i]['productId'],
                      "option1" =>$parentProducts[$i][0][$j]['Weight'],
                      "option2" =>$parentProducts[$i][0][$j]['Height'],
                      "option3" =>$parentProducts[$i][0][$j]['Depth'],
                      "inventory_management"=>"shopify",
                      "position" => "1",
                      "price" => $parentProducts[$i][0][$j]['PricePerPiece'],
                      "inventory_quantity" => "3");
                      array_push($productPayload['variants'],$data);
                  } 
                }
              }else{
                $productPayload['variants'][] = array( 

                "id" =>$parentProducts[$i]['productId'],
                "parent_id" => $parentProducts[$i]['productId'],
                "option1" =>$parentProducts[$i]['Weight'],
                "option2" =>$parentProducts[$i]['Height'],
                "option3" =>$parentProducts[$i]['Depth'],
                "inventory_management"=>"shopify",
                "position" => "1",
                "price" => $parentProducts[$i]['PricePerPiece'],
                "inventory_quantity" => "3");
              }

              $productCreateRS = app('App\Http\Controllers\Api\ApiController')->executeShopifyCurl('products.json','POST',json_encode(array('product'=>$productPayload))); 

              if(!empty($productCreateRS->product)){

                echo "shopify import and shopify product Id  = ".$productCreateRS->product->id."<br>";
                foreach ($productPayload['variants'] as $key => $value) {

                  echo "shopify product variant ID  = ".$productCreateRS->product->variants[$key]->id."<br>";

                  $product_id = $productPayload['variants'][$key]['id'];
                  $product_parent_id = $productPayload['variants'][$key]['parent_id'];
                  $shopify_product_id = $productCreateRS->product->id;
                  $shopify_product_variant_id = $productCreateRS->product->variants[$key]->id;
                  $product_name = $productPayload['title'];
                  $quantity = $productPayload['variants'][$key]['inventory_quantity'];
                  $price = $productPayload['variants'][$key]['price'];
                  //insert into database
                  $Product = Product::create([  
                        'product_id' => $product_id,
                        'product_parent_id' => $product_parent_id,
                        'shopify_product_id' => $shopify_product_id,
                        'shopify_product_variant_id' => $shopify_product_variant_id,
                        'product_name' => $product_name ,
                        'quantity'    =>$quantity,
                        'price'    => $price,
                    ]);
                  $Product->save();

                }  
              }else{
                print_r($productCreateRS);
              }
              }//product add not update
            } //product array loop
          }else{
            echo "Selected file does not exists";
          }
        }else{
          echo "file not find";
        }
    }

  /*===== update product variant option in shopify =========*/
  public function updateShopifyProductVariant($product_variant_id,$weight,$height,$depth){

    $productVariantId = $product_variant_id;
    $productBody  = array('variant' => array('id'=>$productVariantId,'option1' => '3.9'));
    $rs = $this->shopifyapi->updateVariant($productVariantId,$productBody);
    print_r($rs);
  }

}
