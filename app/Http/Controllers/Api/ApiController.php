<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    // $shopify_api_key = "428b581ebec7e4a82bcc2a46f7e903e6";
	 //$shopify_api_password = "403bef497153f1031315afaeface3cb1";

	function executeShopifyCurl($endpoint,$method,$payload=null){
		//echo $payload;
        $url = "https://harvey-thompson-limited.myshopify.com/admin/".$endpoint;

	    $curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL =>$url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  //CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 300,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			// CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_HTTPHEADER => array(
		  	"content-type: application/json",
		    "authorization: Basic NDI4YjU4MWViZWM3ZTRhODJiY2MyYTQ2ZjdlOTAzZTY6NDAzYmVmNDk3MTUzZjEwMzEzMTVhZmFlZmFjZTNjYjE="
		  ),
		));
		if($method=='POST' || $method=='PUT'){
			curl_setopt($curl,CURLOPT_CUSTOMREQUEST,$method);
	    	curl_setopt($curl,CURLOPT_POSTFIELDS,$payload);
		}elseif($method=='DELETE'){
	  		curl_setopt($curl,CURLOPT_CUSTOMREQUEST,$method);  
		}
	 	$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
	  		return array('errorType'=>'curl','error'=>$err);
		} else {
	  		return json_decode($response);
		}
	}

	function escapeJsonString($value) {
	    # list from http://www.json.org/ (\b backspace, \f formfeed)    
	    $escapers =     array("\\",     "/",   "\"",  "\n",  "\r",  "\t", "\x08", "\x0c");
	    $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t",  "\\f",  "\\b");
	    $result = str_replace($escapers, $replacements, $value);
	    return $result;
	}
}
