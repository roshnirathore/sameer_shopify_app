<?php

namespace App\Http\Controllers\Webhooks;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Shopify\ProductAPI;


class DanJewellersController extends Controller
{
    public function __construct()
    {
        $this->api = ProductAPI::init();
    }

    public function handleStockUpdate()
    {      
        $productsId = 4317016031277;
        $productBody  = array('product' => array('id'=>$productsId,'title' => 'Updated via APi'));
        $rs = $this->api->updateProduct($productsId,$productBody);
        print_r($rs);

    }
}
