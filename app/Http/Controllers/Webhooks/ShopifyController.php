<?php
 
namespace App\Http\Controllers\Webhooks;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Shopify\ProductAPI;
use App\Services\DanJewellers\OrderAPI; 
use App\Models\Product;

class ShopifyController extends Controller
{
   
    public function __construct()
    {
        $this->danJjewellersapi = OrderAPI::init();
    }

    /*==== When new order placed in shopify.Send supplier =====*/
    public function handleNewOrder(Request $request)
    {       
        $json = (array) $request->json()->all();

        $order_number = $json['order_number'];
        $first_name = $json['billing_address']['first_name'];
        $last_name = $json['billing_address']['last_name'];
        $customer_name = $first_name." ".$last_name;

        $address1 = $json['shipping_address']['address1'];
        $city = $json['shipping_address']['city'];
        $zip = $json['shipping_address']['zip'];
        $country = $json['shipping_address']['country'];
        $delivery_address = $address1.", ".$city.", ".$country." ".$zip;

        $line_items = $json['line_items']; 
        //when product file from db
            $filepath6 = public_path().'/supplier_new_oder6.txt';
            $file6 = fopen($filepath6,"a");
            fwrite($file6,print_r($line_items ,true));
            fclose($file6);

            $line_items_count = count($line_items)."=====";
            $filepath7 = public_path().'/supplier_new_oder7.txt';
            $file7 = fopen($filepath7,"a");
            fwrite($file7,print_r( $line_items_count,true));
            fclose($file7);


        for ($i=0; $i < count($line_items); $i++) { 
            $shopify_product_id = $line_items[$i]['product_id'];

            $product = Product::where('shopify_product_id',$shopify_product_id)->get();
            $product_arr = $product->toArray();

            //when product file from db
            $filepath3 = public_path().'/supplier_new_oder3.txt';
            $file3 = fopen($filepath3,"a");
            fwrite($file3,print_r($product_arr ,true));
            fclose($file3);

            if(!empty($product_arr)){
                $suppiler_prduct_id  = $product_arr[0]['product_id'];
                $quantity = $line_items[$i]['quantity'];
                $title = $line_items[$i]['title'];

            //when product id and quantity add
            $data4 = "Product_id = > ". $suppiler_prduct_id ." =>Quantity =>". $quantity."</br>";
            $filepath4 = public_path().'/supplier_new_oder4.txt';
            $file4 = fopen($filepath4,"a");
            fwrite($file4,print_r($data4 ,true));
            fclose($file4);

                $word1 = "Watch"; 
                $word2 = "watch";
                // Test if string contains the word 
                if(strpos($title, $word1) !== false || strpos($title, $word2) !== false){
                    echo "Word Found!";

                    $filepath = public_path().'/supplier_new_oder.txt';
                    $file1 = fopen($filepath,"a");
                    fwrite($file1,print_r($order_number ,true));
                    fclose($file1);
                    
                } else{
                    $order = array('OrderXml' => '<Order>
                        <OrderHeader>
                        <PurchaseOrderNumber>"'.$order_number.'"</PurchaseOrderNumber>
                        <Notes>Placed via API</Notes>
                        <CustomerName>H and T Pawnbrokers PLC</CustomerName>
                        <DeliveryAddress>Times House, Throwley Way, Sutton, SM1 4AF</DeliveryAddress>
                        </OrderHeader>
                        <OrderLines>
                        <OrderLine ProductID="'.$suppiler_prduct_id.'" Quantity="'.$quantity.'"/>
                        </OrderLines>
                    </Order>'
                    ); 
                    $rs = $this->danJjewellersapi->createOrder($order);
                    echo "Created Order Id:";
                    print_r($rs);

                    $supplier_data = $rs."</br>";

                    $filepath = public_path().'/supplier_new_oder.txt';
                    $file1 = fopen($filepath,"a");
                    fwrite($file1,print_r($supplier_data ,true));
                    fclose($file1);
                }
                $filepath5 = public_path().'/supplier_new_oder5.txt';
                $file5 = fopen($filepath5,"a");
                fwrite($file5,print_r($order ,true));
                fclose($file5);
            } 

        }
 
        $filepath = public_path().'/new_oder.txt';
        $file = fopen($filepath,"a");
        fwrite($file,print_r($json ,true)); 
        fclose($file);


    }
}
