<?php

namespace App\Http\Controllers;

use GuzzleHttp;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\csvProduct;
use App\Models\Collection;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use App\Services\Shopify\ProductAPI;

class SupplierController extends Controller
{	
	public function __construct(){
       // $this->middleware('auth');
        $this->shopifyapi = ProductAPI::init();
    }

    /*========= Get data when cron run ==================*/
    public function getAllProductPrice(){
        dd();
      $client = new \GuzzleHttp\Client();
	  	$request = $client->get('http://api.danjewellers.co.uk/GetCostPrices.aspx?UserName=HT846247&Password=hL724fc9a532');

		 $rs = $request->getBody();
		 $xml   = simplexml_load_string($rs, 'SimpleXMLElement', LIBXML_NOCDATA);

        $array = json_decode(json_encode($xml), TRUE);
        
        $filepath = public_path().'/update_product_price.txt';
        $file = fopen($filepath,"a");
        fwrite($file,print_r($array ,true));
        fclose($file);

        foreach ($array['Product'] as $key => $value) {
        	 $product_id = $value['@attributes']['ID'];
        	 $price = $value['@attributes']['Price'];

        	$product = Product::where('product_id',$product_id)->get();
        	$product_arr = $product->toArray();
        	if(!empty($product_arr)){
        		$product_variant_id  = $product_arr[0]['shopify_product_variant_id'];
        		$updateResult = $this->updateShopifyProduct($product_variant_id,$price);
        		print_r('price updated');
        	}else{
        		echo "not found"."<br>";
        	}

       
        }	

        

    }
   /*========= update price in shopify ===============*/
    public function updateShopifyProduct($product_variant_id,$price)
    {
	   $productVariantId = $product_variant_id;

    	$productBody  = array('variant' => array('id'=>$productVariantId,'price' => $price));
   		$rs = $this->shopifyapi->updateVariant($productVariantId,$productBody);
    	/*print_r($rs);*/
    	return response()->json($rs);
  	}

    /*====== update quantity by supplier webhook in shopify =====*/
    public function updateProductQuantity(Request $request){

        $json = (array) $request->json()->all();

        $filepath = public_path().'/update_product_quantity.txt';
        $file = fopen($filepath,"a");
         fwrite($file,print_r($json ,true));
         fwrite($file,print_r($json['productId']));
        fclose($file);

        $product_id =  $json['productId'];

        $product = Product::where('product_id',$product_id)->get();
        $product_arr = $product->toArray();
        if(!empty($product_arr)){
            $product_variant_id  = $product_arr[0]['shopify_product_variant_id'];
           
        }

        $quantity =  $json['quantity'];

        $productBody  = array('variant' => array("id"=>$product_variant_id,"inventory_quantity"=>$quantity));
        $rs = $this->shopifyapi->updateVariantQuantity($product_variant_id,$productBody);
        return response()->json($rs);
    }

    /*========== store csv file into database ===============*/
    public function importCsv(){
        return view('csv_import');
    }
    public function saveCsv(Request $request){
        $request->validate([
            'csv_file' => 'required'
        ], [
            'csv_file.required' => 'Please select product csv file'
        ]);
        $file = $request->file('csv_file');
        if($file){
          $extension = $file->getClientOriginalExtension();
          $fileName  = $file->getFilename().'.'.$extension;
          $success = Storage::disk('public')->put($fileName ,  File::get($file));
          $filePath = public_path($fileName);
          if($filePath){
            $csvData=array_map('str_getcsv',file($file));
             //print_r($csvData[1]);
            unset($csvData[0]); //remove header 
            foreach($csvData as $row){

               $csvProduct = csvProduct::create([  
                    'ProductID'        => $row[0],
                    'ParentProductID'  => $row[1],
                    'ProductName'      => $row[2],
                    'OptionName'       => $row[3],
                    'ShortDescription' => $row[4] ,
                    'LongDescription'  => $row[5],
                    'Weight'           => $row[6],
                    'Height'           => $row[7],
                    'Width'            => $row[8],
                    'Depth'            => $row[9],
                    'CostPrice'        => $row[10],
                    'SellPrice'        => $row[11],
                    'Category'         => $row[12],
                    ]);
                  $csvProduct->save();
            }
          }  
        }
        return back()->with('success','Import successfully!');
    }
     
    public function importCsvList(){
        $csvProducts = csvProduct::get();
        $collections = Collection::get();
     return  view('csv_import_list',compact('csvProducts','collections'));  
    }

    public function test(Request $request){
       $productType = $request->input('productType');
       $productTag = $request->input('productTag');
       $collectionId = $request->input('collectionId');

       $csvProductList = csvProduct::whereIn('id',$request->input('id'))->get();
       $result = $csvProductList->toArray();
       $parentProducts = array();
       $childProducts = array();

        foreach($result as $row){
              $product = $row;
              if(empty(trim($product['ParentProductID'])) || $product['ParentProductID'] == 'NULL'){
                  $parentProducts[] = $product;
              }
              if(!empty(trim($product['ParentProductID'])) && trim($product['ParentProductID']) > 0){
                  $childProducts[trim($product['ParentProductID'])][] = $product;
              }  
            }

            for ($i=0; $i < count($parentProducts) ; $i++) { 
              $parentId = $parentProducts[$i]['ProductID'];
              if (array_key_exists($parentId,$childProducts)){
                array_push($parentProducts[$i],$childProducts[$parentId]);  
              }
            }
            print_r($parentProducts);
    }
}
