@extends('layouts.app')
 
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                
                <div class="card-header">CSV Product List</div>
               
                    <form method="POST" action="{{route('test')}}">
                    @csrf
                
                <div class="extra-fields">
                    <div class="collection-list">
                        <select name="collectionId">
                            <option value="">Select</option>
                            @if(isset($collections))
                                @foreach($collections as $collection)
                                    <option value="{{$collection->collection_id}}">{{$collection->title}}</option>
                                @endforeach
                            @endif
                        </select>
                        <div class="product-tag">
                            <label>Product Tag</label>
                            <input type="text" name="productTag">
                        </div>
                        <div class="product-type">
                            <label>Product Type</label>
                            <input type="text" name="productType">
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table>
                        <tr>
                            <th>Select</th>
                            <th>ProductID</th>
                            <th>ParentProductID</th>
                            <th>ProductName</th>
                            <th>LongDescription</th>
                            <th>Weight</th>
                            <th>Height</th>
                            <th>Width</th>
                            <th>SellPrice</th>
                        </tr>
                        @foreach($csvProducts as $csvProduct)
                            <tr>
                                <td><input type="checkbox" name="id[]" value="{{ $csvProduct->id}}"></td>

                                <td>{{ $csvProduct->ProductID}}</td>

                                <td>{{ $csvProduct->ParentProductID}}</td>

                                <td>{{ $csvProduct->ProductName}}</td>

                                <td>{{ $csvProduct->LongDescription}}</td>

                                <td>{{$csvProduct->Weight}}</td>

                                <td>{{ $csvProduct->Height}}</td>

                                <td>{{ $csvProduct->Width}}</td>

                                <td>{{ $csvProduct->SellPrice}}</td>
                            </tr>
                        @endforeach
                       
                    </table>

                     
                    <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
