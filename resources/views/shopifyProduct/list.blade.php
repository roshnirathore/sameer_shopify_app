@extends('layouts.app')
 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Product List</div>
                <div class="card-body">
                    <table border="1">
                        <tr>
                            <th>Product ID</th>
                            <th>Product Name</th>
                            <th>Action</th>
                        </tr>
                        @foreach($products as $product)
                            <tr>
                                <td>{{ $product->product_id}}</td>
                                <td>{{ $product->product_name}}</td>
                                <td> <a href="" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger btn-circle" onclick="comfrimDelet(event,this)">Delete</a></td>
                            </tr>
                        @endforeach
                       
                    </table>
                    <div class="pagination">
                      {{ $products->links() }}
                    </div>
                    <div class="return_page">
                        <p>Go To:</p>
                        <a href="{{route ('importForm')}}">product Form</a>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script type="text/javascript">
  function comfrimDelet(e,element) {
    e.preventDefault();
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this Product!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {     
          window.location.href = $(element).attr('href');
      }
    })
  }
</script>
