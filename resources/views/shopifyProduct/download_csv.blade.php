
    <style type="text/css">
        div#example23_filter  {
         display: none;
        }
        div#example23_info {
         display: none;
        }
        div#example23_paginate{
            display: none;
        }
        table#example23 {
            display: none;
        }
    </style>
<table id="example23">
    <thead>
        <tr>
            <th>ProductID</th>
            <th>ParentProductID</th>
            <th>ProductName</th>
            <th>OptionName</th>
            <th>ShortDescription</th>
            <th>LongDescription</th>
            <th>Weight</th>
            <th>Height</th>
            <th>Width</th>
            <th>Depth</th>
            <th>CostPrice</th>
            <th>SellPrice</th>
            <th>Category</th>
            <th>AvailStk</th>
        </tr>
    </thead>
    <tbody>
        <tr>
        <td>43</td>
        <td>NULL</td>
        <td>Ring</td>
        <td></td>
        <td>9ct Yellow Gold Diamond Cross Pendant</td>
        <td>A stunning cross channel set witd 0.40ct of Diamonds.</td>
        <td>2</td>
        <td>3.5</td>
        <td>NULL</td>
        <td>2.3</td>
        <td>250.58</td>
        <td>350.58</td>
        <td>Diamond > Crosses</td>
        <td>3</td>  
    </tr>
    <tr>
        <td>705</td>
        <td>NULL</td>
        <td>Chain</td>
        <td></td>
        <td>9ct Yellow Gold Diamond Cut Belcher Chain - 18 inch</td>
        <td>Diamond-cut to enhance brilliance, tdis tightly linked chain is suitable for pendants.</td>
        <td>6</td>
        <td>5.5</td>
        <td>6.5</td>
        <td>NULL</td>
        <td>NULL</td>
        <td>NULL</td>
        <td>Diamond > Crosses</td>
        <td>3</td>  
    </tr>
    <tr>
        <td>706</td>
        <td>705</td>
        <td>Chain</td>
        <td></td>
        <td>9ct Yellow Gold Diamond Cut Belcher Chain - 18 inch</td>
        <td>Diamond-cut to enhance brilliance, tdis tightly linked chain is suitable for pendants.</td>
        <td>6</td>
        <td>5.5</td>
        <td>6.5</td>
        <td>NULL</td>
        <td>NULL</td>
        <td>NULL</td>
        <td>Diamond > Crosses</td>
        <td>3</td>  
    </tr>
</tbody>
</table>
                               
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script>
        $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'csv'
            ]
        });
    </script>

