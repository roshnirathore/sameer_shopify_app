<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCsvProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */ 
    public function up()
    {
        Schema::create('csv_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ProductID');
            $table->string('ParentProductID')->nullable();
            $table->string('ProductName');
            $table->string('OptionName')->nullable();
            $table->string('ShortDescription')->nullable();
            $table->string('LongDescription')->nullable();
            $table->string('Weight')->nullable();
            $table->string('Height')->nullable();
            $table->string('Width')->nullable();
            $table->string('Depth')->nullable();
            $table->string('CostPrice')->nullable();
            $table->string('SellPrice')->nullable();
            $table->string('Category')->nullable();
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('csv_products');
    }
}
