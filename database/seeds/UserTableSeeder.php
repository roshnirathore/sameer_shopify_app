

<?php

class UserTableSeeder extends Seeder
{

public function run()
{
    DB::table('users')->delete();
    User::create(array(
        'name'     => 'Ankit Porwal',
        'email'    => 'ankit@geeksperhour.co',
        'password' => Hash::make('ankit001'),
    ));
}

}
